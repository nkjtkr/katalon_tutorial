<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Visit Date Text box</description>
   <name>text_visit_date</name>
   <tag></tag>
   <elementGuidId>05cf1fec-b4a8-4dea-abd4-ee3ac271ad54</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#txt_visit_date</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
