<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>MedicAid Selection Button</description>
   <name>radio_program_medicaid</name>
   <tag></tag>
   <elementGuidId>2a83c4c3-4423-4d92-9b68-c666efc5b303</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#radio_program_medicaid</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
